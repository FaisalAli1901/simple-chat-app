import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Meteor } from 'meteor/meteor';

import './main.html';

var name = 'Anonymous';

Template.messages.helpers({
	messages() {
        return Messages.find({}, { sort: { time: 1 } });
    }
});

Template.input.events({
	'click .send'(e) {
        e.preventDefault();
        var message = document.getElementById("message").value
        
        if (message != "") {
            Messages.insert({
                name: name,
                message: message,
                time: Date.now()
            });  
        }

        document.getElementById('message').value = '';
        message.value = '';
    }
});

Template.welcome.events({ 
    "click .setname"(e) {
        e.preventDefault();
        name = document.getElementById("name").value
        console.log(name);

        document.getElementById('name').value = '';
    } 
});